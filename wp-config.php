<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '895cb9e311cdfad919cc159a94b5725cb5602e701c7b5797');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g?>iHJHE-zR& lx,YJ$`;??EqzC]Sn6%CH[S8a|if,Hn6u:fw+;:D,v4R*Z~6 K2');
define('SECURE_AUTH_KEY',  'fQs{K$:iW/8@]*^i[^[nlxZIgUCV1_fo&oxc+ZXe-I`bn-Xk:^=!eFT-Jk>4j7<t');
define('LOGGED_IN_KEY',    ' QzBqoV8F;~:|IhP9no[;I021TUMW!:T{y@oyTy44xMDZ/)%pEX0Rekw*])|o`UP');
define('NONCE_KEY',        'vhMo`n-?OMwxNnoeKtv.ttf:naB)E[xPG%c1qICwh?x;3`6?q;dqPNJ;89)k495W');
define('AUTH_SALT',        'X(Wb)ldkE)a}xpE%jYKnxdIMc#wvDNsK/~MK};(2a@Y1<;3rWwPCc}:/Cx>u!SuG');
define('SECURE_AUTH_SALT', 'L->>;Wy2^%haxhu8k&@KogLen=7W9:@&<_0UK(<Q/Us(%$^=li7mFX4U+);)WfGa');
define('LOGGED_IN_SALT',   '=Pw3Q!=!-D>J(U@m^_gsS.4q!Zq(jqU%{5b;/L1@Xx,FA3k[PRC<UZ`pY-M04q.3');
define('NONCE_SALT',       'C9Q)PQ7#UdMVc1A$jiDBf(R_~4MZg^m,kL-.QLIz+6Sfo+Gltu[FwZgMj46^>IK]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
